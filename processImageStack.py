###
### This script is deprecated and superseded by "CorrectFIBShading.java"
### As of 2014/10/28, this script is no longer updated, and all changes
### will be made to the java implementation of this technique
###
# 
#  ****************************************************************************
# 
#   Copyright 2016 Joshua Taillon
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
# 
#       http://www.apache.org/licenses/LICENSE-2.0
# 
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
# 
#  ****************************************************************************
#                                                                                 
## This script is meant to be run in ImageJ (Fiji) to process an image stack
#  It will open a stack, reslice it, correct the intensity, and return the corrected
#  stack in the original orientation. The only input required are the voxel sizes.

## Version 1.0.2, 2014/10/28
## Recent changes:
#   - added customized saturation percentage prompt
# Version history:
#   1.0.1, 2014/10/07
#       - 2014/10/07
#       - added copyright statement and comments to code
#   1.0
#       - 2014/10/07
#       - initial script writing and commit

# Import sin and degrees functions, as well as sys commands (for testing) and ImageJ packages
from ij import IJ
from math import sin, degrees
import sys
from time import sleep

# Open image stack with file dialog
imp = IJ.openImage()

# Generate name for corrected stack
orig_name = imp.getTitle()
corr_name = orig_name[:-4] + ".corrected.tif" + ""

# Duplicate original image and hide it
imp_orig = imp.duplicate()
imp_orig.setTitle(orig_name)
imp_orig.hide()

# Get voxel width from user and set to image calibration
xVox = IJ.getNumber("X voxel size", 0)
imp.getCalibration().pixelWidth=xVox

# Assuming a 52 degree inclination, set default pixel height
# confirm this value and ask for voxel depth
yVox = xVox/0.7880107536
yVox = IJ.getNumber("Y voxel size", yVox)
zVox = IJ.getNumber("Z voxel size", 20)

# Set calibrations for voxel sizes
# imp.getCalibration().pixelWidth=xVox                                  # not necessary since we did this above
imp.getCalibration().pixelHeight=yVox
imp.getCalibration().pixelDepth=zVox

# Show the original image, reslice it from the top direction
# and avoid interpolation (single voxel image spacing)
# Also, close original image
imp.show()
IJ.run(imp,"Reslice [/]...", "output=20.000 start=Top rotate avoid")  	# this assumes default z-spacing 
imp.close()

# Get resulting image and set the stack size
imp = IJ.getImage()
n = imp.getStackSize()

# Get saturation percentage for enhance contrast function
satPer = IJ.getNumber("Saturation percentage", 10)

# Loop through the new image stack, and run the "enhance contrast" function
# on each image, with the "normalize" option checked. This will ensure that
# each image will have the same distribution of grayscale values (i.e. even
# illumination)
for i in range(0,n):
    imp.setSlice(i+1)
    IJ.run(imp, "Enhance Contrast", "saturated=" + str(satPer) + " normalize")

# Run the reslice command to get the image back to the original orientation
# and close the first resliced image
IJ.run(imp,"Reslice [/]...", "output=20.000 start=Left avoid")
imp.close()

# Get the new image, set its title and its units
imp = IJ.getImage()
imp.setTitle(corr_name)
imp.getCalibration().setUnit("nm")

# Hide and show both images for comparison. User can choose to save the result, or not
imp.hide()
imp_orig.show()
imp.show()
