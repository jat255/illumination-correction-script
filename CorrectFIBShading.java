/** 
 * CorrectFIBShading.java
 * Purpose: Corrects the vertical gradient in illumination intensity in FIB-SEM stacks.
 * 
 * ****************************************************************************
 *
 *   Copyright 2016 Joshua Taillon
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 * ****************************************************************************
 *
 *
 * Originally written by Joshua Taillon <jat255@gmail.com>, October, 2014
 *
 * This script is meant to be run in ImageJ (Fiji) to process an image stack
 * It will open a stack, reslice it, correct the intensity, and return the corrected
 * stack in the original orientation. The only input required are the voxel sizes.
 *
 * Originally written in Python, but rewritten in java to take advantage of included
 * normalization script, which is more easily implemented in Fiji's native Java 
 *
 * @author Joshua Taillon
 * @version 1.1
 *
 * Version 1.1, 2014/10/28
 *	Recent changes:
 *  	- changed image normalization technique to method from X. Dai paper (1998)
 *  	- switched to Java as implementation language
 *  	- script is now named "CorrectFIBShading.java"
 *  	- normalizes once in original image slicing direction, and then in vertical
 *  	  direction to correct for the primary shading artefact
 *	
 *	Version history:
 *		1.0.2, 2014/10/28
 *			- added customized saturation percentage prompt
 *  	1.0.1, 2014/10/07
 *      	- added copyright statement and comments to code
 *  	1.0,   2014/10/07
 *      	- initial script writing and commit
 *      
 */

import ij.*;
import ij.plugin.filter.*; 
import ij.process.*;
import ij.measure.*;

/** 
 * CorrectFIBShading
 * Corrects the vertical gradient in illumination intensity in FIB-SEM stacks.
*/
public class CorrectFIBShading implements PlugInFilter { 
	/********************************
			Helper methods:
	********************************/
	
	/** 
    * Close an image without any sort of confirmation
    * 
    * @param IP, the ImagePlus to close.
    */ 
	public void closeIm(ImagePlus IP){
		IP.changes = false;
		IP.close();
	}

	/** 
    * Copy the calibration from one ImagePlus to another.
	*
    * @param IPFrom, the ImagePlus from which to copy calibration;
    * IPTo, the ImagePlus to which to copy the calibration
    */ 
    public void copyCalibration(ImagePlus IPFrom, ImagePlus IPTo){
   		IPTo.getCalibration().pixelWidth = IPFrom.getCalibration().pixelWidth;
        IPTo.getCalibration().pixelHeight = IPFrom.getCalibration().pixelHeight;
        IPTo.getCalibration().pixelDepth = IPFrom.getCalibration().pixelDepth;
        IPTo.getCalibration().setUnit(IPFrom.getCalibration().getUnit());
    }

    /**
    * setup function to set up this PluginFilter
    */
    private ImagePlus IP; 
    private int curSlice;
    public int setup(String args, ImagePlus IP) { 
        // if there is no image loaded, display message declaring so
        if (IP == null) 
            IJ.noImage();
        this.IP = IP; 
        // save current slice number so we can use it as reference
        curSlice = IP.getCurrentSlice(); 
        return DOES_8G; 
    } 

    /********************************
			Primary methods:
	********************************/

    /** 
     * Return a new ImageStack with the normalized ilumination. The 
     * return stack is generated from original stack 
     * 
     * @param refSlice, the number of the frame to use as intensity reference. 
     * @return Returns a corrected ImageStack with the ilumination normalized from slice to slice. 
     * 
     * Normalization is accomplished using equation (3) of: 
     * Dai, X. (1998). "The effects of image misregistration on the accuracy of remotely 
     * sensed change detection." IEEE Transactions on Geoscience and Remote Sensing, 
     * 36(5), 1566–1577. doi:10.1109/36.718860
     * 
     */ 
    public ImageStack stackIlluminationNorm(ImageStack stack, int refSlice) { 
        int Z = stack.getSize(); 		// stack depth (number of images)
        int X = stack.getWidth(); 		// stack width (width of each image)
        int Y = stack.getHeight(); 		// stack height (height of each image)

    	if (Z == 1) { 
        	throw new IndexOutOfBoundsException("Stack must be larger than 1 image"); 		// if not really a stack, just return
    	}
      

        ImageStack newStack = new ImageStack(X, Y); 	// create new stack with same size as original

       	// create temporary calculation image and convert image to 32 bit float
        ImageProcessor calcImage;
        ImageProcessor referenceImage = stack.getProcessor(refSlice).convertToFloat(); 

		// create statistics object to get detailed information about the reference image 
        FloatStatistics stats = new FloatStatistics(referenceImage); 
        double sig_ref = stats.stdDev; 				// stdDev of reference image
        double mu_ref = stats.mean; 				// mean value of referenceimage
        double sig_old; 							// variables to hold σ and μ of each image
        double mu_old; 								// as we progress through the stack

		// progress through the stack: 
        for (int i = 1; i <= Z; i++) { 
            calcImage = stack.getProcessor(i).convertToFloat(); 			// convert each image to 32 bit float
            stats = new FloatStatistics(calcImage); 						// get stats for each image
            sig_old = stats.stdDev; 			// stdDev and mean are relevant
            mu_old = stats.mean; 				// statistics needed for the correction

            /* 	normalize each image ("old", below) to reference image ("ref", below) 
               	using equations from paper cited above:
               	I_new = σ_ref/σ_old * {I_old - μ_old} + μ_ref
           	*/ 
            calcImage.subtract(mu_old);
			calcImage.multiply(sig_ref/sig_old);
			calcImage.add(mu_ref);
                
            newStack.addSlice(i + "", calcImage.convertToByte(false)); 
            IJ.showProgress(i, Z); 
  		}  
        return newStack; 
    } 

    /**
	 * Runs the actual correction script, calling the normalization method and "reslice" tool within
	 * Fiji as necessary to correct in two orthogonal dimensions.
	 */
    public void run(ImageProcessor ip) {
    	try{
	    	String baseName = IP.getTitle().substring(0,IP.getTitle().length()-3);
	    	
	    	// Do initial intensity correction in original image orientation
	    	IJ.showStatus("Doing intial intensity normalization");
	    	ImagePlus out1 = new ImagePlus(baseName + "corrected1", stackIlluminationNorm(IP.getImageStack(), curSlice)); 
	        copyCalibration(IP, out1);
	        out1.show("1st normalization done"); 
			closeIm(IP);
			IP = IJ.getImage();
	
	        // Reslice from top in order to correct FIB-shading
	        IJ.run(IP,"Reslice [/]...", "output=20.000 start=Top avoid");  	// this assumes default z-spacing
			closeIm(IP);
			IP = IJ.getImage();
	
			// Perform intensity normalization on resliced image stack
	        IJ.showStatus("Doing intensity normalization on resliced image");
	        ImagePlus out2 = new ImagePlus(baseName + "corrected2", stackIlluminationNorm(IP.getImageStack(), 1)); 
	        copyCalibration(IP,out2);
	        out2.show("2nd normalization done"); 
	        closeIm(IP);
	        IP = IJ.getImage();
	
	        // Final reslice on corrected image to get back to original orientation
	        IJ.run(IP,"Reslice [/]...", "output=20.000 start=Top avoid");  	
	        closeIm(IP);
	        IP = IJ.getImage();
	        IP.setTitle(baseName + "shade-corrected");
	        IP.show();
    	} catch (Exception e){
    		// if any exceptions are thrown, display information and exit immediately
    		IJ.handleException(e);
    		return;
    	}
    } 
} 
