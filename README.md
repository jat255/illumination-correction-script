# README #

### What is this repository for? ###

* This repository is to hold scripts and relevant files to Joshua Taillon's illumination correction script for correction of shading in FIB/SEM stacks
* Current version: 1.1

### How do I get set up? ###

* Requires a current version of Fiji (Image J 2.0) to run.  Open a stack of images to correct, navigating to the slice that you would like to use as the reference illumination. Then load the Java script into Fiji, compile it, and run it. 
* To install in Fiji, open the .java file in the script editor, and export as a JAR, ensuring there is an underscore in the name. Add plugins.config to this jar (with the appropriate information to locate the plugin in the menus, then add the jar to the plugins\jars folder of Fiji.

### Who do I talk to? ###

Maintainer: Joshua Taillon (jat255@gmail.com)